
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>


<?php

$connection = new mysqli('localhost', 'root', '', 'abc');

//amar submit button ti click kora thakle amar isset kora thakte hobe..ekhane isset mane hocche jod submit button e click kora hoy, 
//amrra kicu value recieve korbo, jmn name,roll, dept.

if(isset($_POST['add_student'])){

    #get form values
//jehetu data gulu dhorte parci,
    $name = $_POST ['name'];
    $roll = $_POST ['roll'];
    $dept = $_POST ['dept'];

    if (empty($name) || empty($roll) || empty($dept)) {

        $msg = "<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">
        <p>All fields are required!</p>
        <button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"alert\" aria-label=\"Close\"></button>
        </div>" ;
        
    }else {

        //akhon amra database e data gulu pathay dibo.
        

        $connection -> query ("INSERT INTO students(name, roll, dept) VALUES('$name', '$roll', '$dept' )");

        $msg = "<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">
        <p>Data Stable!</p>
        <button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"alert\" aria-label=\"Close\"></button>
        </div>" ;
    
    } 
} 

//form theke evabe data gulu database e send korte pari.........

?>

<div class="containr  my-5">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card shadow">
                <div class="card-body">
                    <h2>Create your account</h2>

                    <!--$msg variable, tmr moddhe jodi kono value thake, show koraba, na hole empty value diba. -->
                    <?php echo $msg ?? '';?> 
                    
                    <hr>
                    <form action="" method="POST">
                    <div class="my-3">
                        <label for="">Name</label>
                        <input name = "name" type="text"class="form-control">
                        </div>

                        <div class="my-3">
                        <label for="">Roll</label>
                        <input name = "roll" type="text"class="form-control">
                        </div>

                        <div class="my-3">
                            <label for="name">Dept.</label>
                            <select class="form-control" name="dept" id="">
                                <option value="">--Select--</option>
                                <option value="CSE">CSE</option>
                                <option value="EEE">EEE</option>
                                <option value="BBA">BBA</option>
                                <option value="MBA">MBA</option>
                                
                            </select>
                        </div>

                        <div class="my-3">
                        <input type="submit" name="add_student" class="btn btn-primary w-100" value="Add New Student">
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="col-md-5">
            <div class="card shadow">
                <div class="card-body">
                    <h2>All Students Data</h2>
                    <hr>
                    <table class="table">
                        <thead>
                            <tr>
                               <th>#</th>
                               <th>Name</th>
                               <th>Roll</th>
                               <th>Dept</th>
                               <th>Action</th>

                            </tr>

                        </thead>

                        <tbody>


                        <!--database theke data gulu dhorte hobe,select query er 
                        maddhome amara database theke data gulu dhorte pari -->

                         <?php

                         //coonection variable e amar database ta connet kora ache, 
                         //shei database k call kore ami ekta query chalacchi, select query data gulu k select korar jonno.
                         
                         $data = $connection -> query("SELECT * from students" );
                         echo "Students number = " . $data -> num_rows;  //etar mane hoccche table e amar koto gulu data ache sheta dekhabe. data fetch korar age boshate obe obosshoi..

                         $i = 1; // i er initial man nici 1, na hole id er number gulu elomelo ashto..

                         //data ta k dhore feleci, akhon readabale korte chaile amk fetch korte hobe...
                         //while loop er moddhe ami data ta k fetch korbo..and student nam er variable e rakhbo..

                         while ($student = $data -> fetch_object()):    // data 3 vabe fetch kora jay, 
                                                                        //1) fetch array()
                                                                        //2) fetch object()
                                                                        //3) fetch assoc() associative array.
                         ?>

                         <!--table er moddhe tr e data gulu show korte chai, 
                         tai tr e age amk data gulu dhote hobe...-->

                            <tr>
                                <td><?php echo $i; $i++ ?></td> <!--and prottek bar i er value ek kore arbe..-->
                                <td><?php echo $student -> name; ?></td> <!--student table theke name collumn er data dhorlam-->
                                <td><?php echo $student -> roll; ?></td> <!--student table theke roll collumn er data dhorlam-->
                                <td><?php echo $student -> dept; ?></td> <!--student table theke dept collumn er data dhorlam-->

                                <td>
                                    <a class = "btn btn-sm btn-info" href="#">View</a>
                                    <a class = "btn btn-sm btn-warning" href="#">Edit</a>
                                    <a class = "btn btn-sm btn-danger" href="#">Delete</a>
                                </td>
                            </tr>
                            <?php endwhile ;?>
                         
                        </tbody>
                    </table> 
                </div>
            </div>
        </div>
    </div>
</div>


       
  



<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>